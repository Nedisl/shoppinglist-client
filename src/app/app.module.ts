import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MessageComponent } from './message/message.component';
import {DataService} from './data.service';
import {RestApiService} from './rest-api.service';
import {MessageService} from './message.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './/app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule, NgbModule.forRoot(), AppRoutingModule, HttpClientModule, FormsModule, NgxMaskModule.forRoot()
  ],
  providers: [RestApiService, DataService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
