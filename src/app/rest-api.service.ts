import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

const API_URL = 'http://46.161.39.213:1337';

@Injectable({
  providedIn: 'root'
})


export class RestApiService {

  constructor(private http: HttpClient) { }

  /**
   * Get all items from server
   * @returns {Promise<Object>}
   */
  getItems() {
    return this
      .http
      .get(`${API_URL}/api/items/list`, {})
      .toPromise();
  }

  /**
   * Push item to server
   * @returns {Promise<Object>}
   */
  addItem(body: any) {
    return this
      .http
      .post(`${API_URL}/api/items/add`, body)
      .toPromise();
  }

  /**
   * Delete item from server
   * @returns {Promise<Object>}
   */
  deleteItem(body: any) {
    return this
      .http
      .request('DELETE', `${API_URL}/api/items/delete`, {
        body: {
          _id: body._id
        }
      })
      .toPromise();
  }

}
