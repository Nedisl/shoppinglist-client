import { Injectable } from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {RestApiService} from './rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  message = '';
  messageType = 'danger';

  constructor(private router: Router, private rest: RestApiService) {
    /**
     * Subscribe for messages
     */
    this
      .router
      .events
      .subscribe(event => {
        this
          .router
          .events
          .subscribe(nextEvent => {
            if (nextEvent instanceof NavigationStart) {
              this.message = '';
            }
          });
      });
  }

  /**
   * Show error message
   * @param message
   */
  error(message) {
    this.messageType = 'danger';
    this.message = message;
  }

  /**
   * Show success message
   * @param message
   */
  success(message) {
    this.messageType = 'success';
    this.message = message;
  }
}
