import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {MessageService} from '../message.service';
import {RestApiService} from '../rest-api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  name = '';
  description = '';
  price = 0;

  constructor(public data: DataService, private message: MessageService, private rest: RestApiService) { }

  async ngOnInit() {
    await this
      .data
      .getItems();
  }

  /**
   * Add item to list
   * @returns {Promise<void>}
   */
  async addItem() {
    const data = await this.rest.addItem({
      name: this.name,
      description: this.description,
      price: this.price
    });

    if (data['meta'].success) {
      this.message.success(data['meta'].message);
      await this
        .data
        .getItems();
    } else {
      this.message.error(data['meta'].message);
    }
  }

  /**
   * Delete item from list
   * @param id
   * @returns {Promise<void>}
   */
  async deleteItem(id) {
    const data = await this.rest.deleteItem({
      _id: id
    });

    if (data['meta'].success) {
      this.message.success(data['meta'].message);
      await this
        .data
        .getItems();
    } else {
      this.message.error(data['meta'].message);
    }
  }
}
