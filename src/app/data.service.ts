import { Injectable } from '@angular/core';
import {RestApiService} from './rest-api.service';
import {MessageService} from './message.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  items: any;
  constructor(private rest: RestApiService, private message: MessageService) { }

  /**
   * Get List item
   * @returns {Promise<void>}
   */
  async getItems() {
    try {
        const data = await this
          .rest.getItems();
        this.items = data['data'].items.reverse();
    } catch (error) {
      this.message.error(error);
    }
  }
}
